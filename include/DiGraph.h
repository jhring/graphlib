//<editor-fold desc="Documentation">

/*! \file dgraph.h
 *  \brief Definition of a directed graph class.
 *
 *  Only minor changes to the class Graph are necessary.
 *
 *  \author John H. Ring IV
 *  \date October, 2017
 */

/*! \class DGraph
 * \brief A simple directed graph.
 *
 * Implemented by extending Graph, only slight modification is required.
 * Edges are unweighted with no self loops and at most a single edge is allowed
 * from one node to another node.
 */

/*! \fn std::vector<edge> DGraph::edges();
 *  \brief Getter for edges
 *  \return Vector of all edges in the graph
 */

/*! \fn virtual void DGraph::add_edge(const edge& e)
 * \brief Adds an edge to the graph.
 *
 * If nodes in the edge do not exist they are added to the graph.
 * No action is taken for existing or self edges.
 *
 * \param e edge to add
 */

/*! \fn virtual void DGraph::remove_edge(const edge& e)
 * \brief Removes an edge from the graph.
 *
 * If the edge does not exist in the graph no action is taken.
 *
 * \param e edge to remove
 */

/*! \fn virtual bool DGraph::connected()
 * \brief Checks if graph is connected.
 *
 * A directed graph is connected if there exists a node from which you can get
 * to every other node.
 *
 * \return True if graph is connected.
 */

/*! \fn virtual bool DGraph::strongly_connected()
 * \brief Checks if graph is strongly connected.
 *
 * An directed graph is connected if you can get from any node to any other
 * node in the graph.
 *
 * \return True if graph is connected.
 */

//</editor-fold>

#ifndef GRAPHLIB_DGRAPH_H
#define GRAPHLIB_DGRAPH_H

#include "Graph.h"

class DiGraph: public Graph {
public:
    std::vector<edge> edges() override;
    void add_edge(const edge& e) override;
    void remove_edge(const edge& e) override;
    unsigned long num_edges() override;
    bool connected() override;
    bool strongly_connected();
};


#endif //GRAPHLIB_DGRAPH_H
