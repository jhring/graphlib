//<editor-fold desc="Documentation">

/*! \file graph.h
 *  \brief Definition of a undirected graph class.
 *
 *  \author John H. Ring IV
 *  \date October, 2017
 */

/*! \typedef std::pair<int, int> edge
 *  \brief An edge between two vertices
 *
 *  In a directed graph the edge is from the vertex specified by the first
 *  integer to the second.
*/

/*! \class Graph
 * \brief A simple undirected graph.
 *
 * Implemented with adjacency list. Vertices are integers.
 * Edges are unweighted with no self loops and at most a single edge is allowed
 * between two nodes.
 */

/*! \fn Graph::Graph()
 * \brief Default constructor for Graph.
 *
 * Creates an empty instance of Graph; no nodes, no edges.
 */

/*!  \fn Graph::Graph(std::map<int, std::set<int>>& adj)
 *  \brief Constructs an instance of Graph from an existing Adjacency list.
 *
 *  This operation is not safe, the adjacency list is assumed to be error free.
 *  The preferred method to build a graph is add_node and add_edges.
 *
 *  \param adj the adjacency list
 */

/*! \fn std::vector<int> Graph::nodes()
 * \brief Getter for nodes
 * \return Vector of all nodes in the graph
 */

/*! \fn std::vector<edge> Graph::edges();
 *  \brief Getter for edges
 *
 *  Edges are returned with the highest ID listed first.
 *
 *  \return Vector of all edges in the graph
 */

/*! \fn void Graph::add_node(int id)
 * \brief Adds a node to the graph
 *
 * If the node ID specified is already in the graph no action is taken.
 *
 * \param id The node to add
 */

/*! \fn void Graph::add_nodes(const std::vector<int>& ids)
 * \brief Adds nodes to the graph.
 *
 * Add nodes with no edges to the adjacency list. Existing nodes are skipped.
 *
 * \param ids list of nodes to add
 */

/*! \fn void Graph::remove_node(int id)
 * \brief Removes node from the graph.
 *
 * All edges containing the specified node are also removed.
 * If the node does not exist no action is taken.
 *
 * \param id node to remove
 */

/*! \fn void Graph::remove_nodes(const std::vector<int>& ids)
 * \brief Removes a list of nodes from the graph.
 *
 * All edges connected to specified nodes. No action is taken for nodes that
 * do not exist.
 *
 * \param ids nodes to remove
 */

/*! \fn virtual void Graph::add_edge(const edge& e)
 * \brief Adds an edge to the graph.
 *
 * If nodes in the edge do not exist they are added to the graph.
 * No action is taken for existing or self edges.
 *
 * \param e edge to add
 */

/*! \fn void Graph::add_edges(const std::vector<edge>& e)
 * \brief Adds a list of edges to the graph.
 *
 * If nodes in the edges do not exist they are added to the graph.
 * No action is taken for existing or self edges.
 *
 * \param e edges to add
 */

/*! \fn virtual void Graph::remove_edge(const edge& e)
 * \brief Removes an edge from the graph.
 *
 * If the edge does not exist in the graph no action is taken.
 *
 * \param e edge to remove
 */

/*! \fn void Graph::remove_edges(const std::vector<edge>& e)
 * \brief Removes a list of edges from the graph.
 *
 * No action is taken for edges that do not exist.
 *
 * \param e edges to remove
 */

/*! \fn bool Graph::has_node(int id)
 * \brief Checks if a node is in the graph.
 *
 * \param id node to check
 * \return True if specified node is in the graph
 */

/*! \fn bool Graph::has_edge(const edge& e)
 * \brief Checks if an edge is in the graph.
 *
 * \param e edge to check
 * \return True if e is in the graph
 */

/*! \fn std::vector<int> Graph::neighbors(int id)
 * \brief Gets a list of neighbors to the specified node.
 *
 * Node n1 has a neighbor n2 if there is an edge from n1 to n2.
 * An empty vector is returned if the specified node does not exist.
 *
 * \param id node to check
 * \return neighbors of id
 */

/*! \fn int Graph::num_nodes()
 * \brief Gets the number of nodes in the graph.
 * \return Number of nodes
 */

/*! \fn int Graph::num_edges()
 * \brief Gets the number of edges in the graph.
 * \return number of edges
 */

/*! \fn void Graph::info()
 * \brief Prints the graph as an adjacency list.
 */

/*! \fn std::vector<int> Graph::BFS(int start, int end)
 * \brief Preforms breath first search.
 *
 * Finds a path between start and end if one exists.
 *
 * \param start node to start searching from
 * \param end node to end search at
 * \return path from start to end or empty
 */

/*! \fn std::vector<int> Graph::DFS(int start, int end)
 * \brief Preforms depth first search.
 *
 * Finds a path between start and end if one exists.
 *
 * \param start node to start searching from
 * \param end node to end search at
 * \return path from start to end or empty
 */

/*! \fn virtual bool Graph::connected()
 * \brief Checks if graph is connected.
 *
 * A undirected graph is connected if you can get from any node to any other
 * node in the graph.
 *
 * \return True if graph is connected.
 */

/*! \fn void Graph::read_edgelist(const std::string path, char delimiter)
 * \brief Reads an edgelist into the graph
 *
 * \param path filepath of the edgelist
 * \param delimiter Separator the file uses
 */

/*! \var std::map<int, std::set<int>> Graph::m_adjList
 * The adjacency list that stores the graph.
 */

//</editor-fold>

#ifndef GRAPHLIB_LIBRARY_H
#define GRAPHLIB_LIBRARY_H

#include <set>
#include <string>
#include <utility>
#include <vector>
#include <map>

typedef std::pair<int, int> edge;

class Graph {
public:
    Graph();

    explicit Graph(std::map<int, std::set<int>>& adj);

    bool operator==(Graph& g);

    std::vector<int> nodes();
    virtual std::vector<edge> edges();
    void add_node(int id);
    void add_nodes(const std::vector<int>& ids);
    void remove_node(int id);
    void remove_nodes(const std::vector<int>& ids);
    virtual void add_edge(const edge& e);
    void add_edges(const std::vector<edge>& e);
    virtual void remove_edge(const edge& e);
    void remove_edges(const std::vector<edge>& e);
    bool has_node(int id);
    bool has_edge(const edge& e);
    std::vector<int> neighbors(int id);
    unsigned long num_nodes();
    virtual unsigned long num_edges();
    unsigned long in_degree(int id);
    std::vector<unsigned long> in_degree(std::vector<int> ids = {});
    double clustering(int id);
    std::vector<double> clustering(std::vector<int> ids = {});
    void info();
    std::vector<int> BFS(int start, int end);
    std::vector<int> DFS(int start, int end);
    virtual bool connected();
    void read_edgelist(const std::string& path, char delimiter=' ');

protected:
    std::map<int, std::set<int>> m_adjList;
};

#endif
