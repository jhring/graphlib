//
// Created by jhring on 2/3/18.
//

#include "Graph.h"

#ifndef GRAPHLIB_MODELS_H
#define GRAPHLIB_MODELS_H

Graph complete_graph(unsigned int n);
Graph er_model(unsigned int n, unsigned int M);
Graph configuration_model(std::vector<unsigned long> deg_sequence);


#endif //GRAPHLIB_MODELS_H
