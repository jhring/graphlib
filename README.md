# Description:

This project aims to implement a simple graph library using the STL.
To build simply clone the directory and run `make`.

---

# Dependency List

| Name | Tested Version | Optional  |
| ------------- |:-------------:| :-----:|
| g++ | 7.2.1 |  |
| cmake| 3.4 | |
| make | 4.2.1 | |
| doxygen | 1.18.3 | * 
| graphviz | 2.40.1 | *
| latex | TeX Live 2016 | *

# Documentation:

If Doxygen and additional dependencies such as latex and dot are installed, 
documentation will be generated during the build.

---