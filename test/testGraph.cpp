//
// Created by jhring on 2/3/18.
//

#include "gtest/gtest.h"

#include "../include/Graph.h"
#include "../include/models.h"

TEST(complete_graph, base) {
    Graph g = complete_graph(5);
    ASSERT_EQ(g.num_edges(), 10);
}

TEST(er_model, constructive) {
    Graph g = er_model(5,3);
    ASSERT_EQ(g.num_edges(), 3);
}

TEST(er_model, destructive) {
    Graph g = er_model(5,7);
    ASSERT_EQ(g.num_edges(), 7);
}

TEST(er_model, error) {
    ASSERT_EXIT(er_model(1, 2), ::testing::ExitedWithCode(1), "er_model");
}

TEST(nodes, base) {
    Graph g = er_model(5, 3);
    ASSERT_EQ(g.nodes(), std::vector<int>({0, 1, 2, 3, 4}));
}

TEST(edges, base) {
    Graph g1 = complete_graph(3);
    Graph g2;
    g2.add_edges({{0,1},{0,2},{1,2}});
    ASSERT_EQ(g1.edges(), g2.edges());
}

TEST(add_node, base) {
    Graph g;
    g.add_node(4);
    ASSERT_TRUE(g.has_node(4));
}

TEST(add_node, existing) {
    Graph g = complete_graph(3);
    g.add_node(0);
    ASSERT_TRUE(g.has_edge({0,2}));
}

TEST(remove_node, base) {
    Graph g1 = complete_graph(4);
    Graph g2 = complete_graph(3);

    g1.remove_node(3);
    ASSERT_TRUE(g1 == g2);
}
