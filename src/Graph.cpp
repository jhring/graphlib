/*! \file graph.cpp
 *  \brief Implementation of a undirected graph class.
 *
 *  \author John H. Ring IV
 *  \date October, 2017 - February 2018
 */

#include <algorithm>
#include <fstream>
#include <iostream>
#include <iterator>
#include <queue>
#include <sstream>
#include <stack>

#include "../include/Graph.h"

static std::vector<std::string> split(const std::string& s, char delimiter) {
    std::vector<std::string> tokens;
    std::string token;
    std::istringstream tokenStream(s);
    while (std::getline(tokenStream, token, delimiter))
    {
        tokens.push_back(token);
    }
    return tokens;
}

Graph::Graph() : m_adjList(std::map<int, std::set<int>>()) {};

Graph::Graph(std::map<int, std::set<int>>& adj) {
    for (auto &it : adj) {
        add_node(it.first);
        for (auto e_it = it.second.begin(); e_it != it.second.end(); ++e_it) {
            add_edge(std::make_pair(it.first, *e_it));
        }
    }
};

bool Graph::operator==(Graph& g) {
    return (edges() == g.edges()) && (nodes() == g.nodes());
}

void Graph::read_edgelist(const std::string& path, char delimiter) {
    std::ifstream file (path);
    std::string line;
    if (file.is_open()) {
        while (getline(file, line)) {
            auto tokens = split(line, delimiter);
            add_edge(std::make_pair(std::stoi(tokens[0]), std::stoi(tokens[1])));
        }
        file.close();
    }
    // ToDo error checking, skip headers
}

std::vector<int> Graph::nodes() {
    std::vector<int> nodes;
    nodes.reserve(num_nodes());
    for (auto &it : m_adjList) {
        nodes.push_back(it.first);
    }
    return nodes;
}

std::vector<edge> Graph::edges() {
    std::set<edge> edges;
    for (auto &it : m_adjList) {
        for (auto e_it = it.second.begin(); e_it != it.second.end(); ++e_it) {
            edges.insert(std::make_pair(std::max(it.first, *e_it), std::min(it.first, *e_it)));
        }
    }
    return std::vector<edge>(edges.begin(), edges.end());
}

void Graph::add_node(int id) {
    if (!has_node(id)) {
        m_adjList.emplace(id, std::set<int>());
    }
}

void Graph::add_nodes(const std::vector<int>& ids) {
    for (auto const& id: ids) {
        add_node(id);
    }
}

void Graph::remove_node(int id) {
    auto const& neigh = neighbors(id);
    for (auto const& node: neighbors(id)) {
        m_adjList[node].erase(id);
    }
    m_adjList.erase(id);
}

void Graph::remove_nodes(const std::vector<int>& nodes) {
    for (int node : nodes) {
        remove_node(node);
    }
}

void Graph::add_edge(const edge& e) {
    // operates as default dict so should work
    if (e.first != e.second) {
        m_adjList[e.first].insert(e.second);
        m_adjList[e.second].insert(e.first);
    }
}

void Graph::add_edges(const std::vector<edge>& e) {
    for (const auto &it : e) {
        add_edge(it);
    }
}

void Graph::remove_edge(const edge& e) {
    if (has_edge(e)) {
        m_adjList[e.first].erase(e.second);
        m_adjList[e.second].erase(e.first);
    }
}

void Graph::remove_edges(const std::vector<edge>& e) {
    for (const auto &it : e) {
        remove_edge(it);
    }
}

bool Graph::has_node(int id) {
    return m_adjList.count(id) == 1;
}

bool Graph::has_edge(const edge& e) {
    return m_adjList[e.first].count(e.second) == 1;
}

std::vector<int> Graph::neighbors(int id) {
    if (has_node(id))
        return std::vector<int>(m_adjList[id].begin(), m_adjList[id].end());
    else
        return std::vector<int>();
}

unsigned long Graph::num_nodes() {
    return m_adjList.size();
}

unsigned long Graph::num_edges() {
    unsigned long num = 0;

    for (auto &it : m_adjList) {
        num += it.second.size();
    }
    return num/2;
}

unsigned long Graph::in_degree(int id) {
    return m_adjList[id].size();
}

std::vector<unsigned long> Graph::in_degree(std::vector<int> ids /*= {}*/) {
    std::vector<unsigned long> degrees;
    if (!ids.empty()) {
        degrees.reserve(ids.size());
        for (auto const &id: ids) {
            degrees.push_back(m_adjList[id].size());
        }
    } else {
        degrees.reserve(num_nodes());
        for (auto const &node: nodes()) {
            degrees.push_back(in_degree(node));
        }
    }
    return degrees;
}

double Graph::clustering(int id) {
    unsigned int triads = 0;
    unsigned int triangles = 0;

    std::vector<int> neigh1 = neighbors(id);
    std::vector<int> neigh2;

    for (int i = 0; i < neigh1.size(); ++i) {
        neigh2 = neighbors(neigh1[i]);
        for (int j = i + 1; j < neigh1.size(); ++j) {
            ++triads;
            if (find(neigh2.begin(), neigh2.end(), neigh1[j]) != neigh2.end())
                ++triangles;
        }
    }
    double ret = -1;
    if (triads > 0)
        ret = triangles / (1.0 * triads);
    return ret;
}

std::vector<double> Graph::clustering(std::vector<int> ids /*= {}*/) {
    std::vector<double> ret;
    if (!ids.empty()) {
        ret.reserve(ids.size());
        for (auto const &id: ids) {
            ret.push_back(clustering(id));
        }
    } else {
        ret.reserve(num_nodes());
        for (auto &it: m_adjList) {
            ret.push_back(clustering(it.first));
        }
    }
    return ret;
}

void Graph::info() {
    for (auto &it : m_adjList) {
        std::cout << it.first << ": ";
        for (int e_it : it.second) {
            std::cout << e_it << " ";
        }
        std::cout << std::endl;
    }
}

std::vector<int> Graph::BFS(int start, int end) {
    std::vector<int> path = {};
    std::vector<int> visited = { start };
    std::queue<std::vector<int>> queue;
    queue.push({ start });

    while (!queue.empty()) {
        path = queue.front();
        int node = path.back();
        queue.pop();
        if (node == end) {
            return path;
        }

        for (auto &entry : neighbors(node)) {
            if (std::find(visited.begin(), visited.end(), entry) == visited.end()) {
                visited.emplace_back(entry);
                std::vector<int> new_path = path;
                new_path.emplace_back(entry);
                queue.push(new_path);
            }
        }
    }
    return {};
}

std::vector<int> Graph::DFS(int start, int end) {
    std::vector<int> path = {};
    std::vector<int> visited = { start };
    std::stack<std::vector<int>> stack;
    stack.push({ start });

    while (!stack.empty()) {
        path = stack.top();
        int node = path.back();
        stack.pop();
        if (node == end) {
            return path;
        }

        for (auto &entry : neighbors(node)) {
            if (std::find(visited.begin(), visited.end(), entry) == visited.end()) {
                visited.emplace_back(entry);
                std::vector<int> new_path = path;
                new_path.emplace_back(entry);
                stack.push(new_path);
            }
        }
    }
    return {};
}

bool Graph::connected() {
    std::vector<int> node = nodes();
    for (auto &n : node) {
        if (node.front() != n)
            if (BFS(node.front(), n).size() == 0)
                return false;
    }
    return true;
}