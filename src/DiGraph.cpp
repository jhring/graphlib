/*! \file dgraph.cpp
 *  \brief Definition of a directed graph class.
 *
 *  \author John H. Ring IV
 *  \date October, 2017
 */

#include "../include/DiGraph.h"

std::vector<edge> DiGraph::edges() {
    std::vector<edge> edges;
    for (auto &it : m_adjList) {
        for (auto &e_it : it.second) {
            edges.emplace_back(std::make_pair(it.first, e_it));
        }
    }
    return edges;
}

void DiGraph::add_edge(const edge& e) {
    // operates as default dict so should work
    if (e.first != e.second) {
        m_adjList[e.first].insert(e.second);
    }
}

void DiGraph::remove_edge(const edge& e) {
    if (has_edge(e)) {
        m_adjList[e.first].erase(e.second);
    }
}

unsigned long DiGraph::num_edges() {
    unsigned long num = 0;
    for (auto const& node: nodes()) {
        num += m_adjList[node].size();
    }
    return num;
}

bool DiGraph::connected() {
    Graph g = Graph();
    g.add_edges(edges());
    return g.connected();
}

bool DiGraph::strongly_connected() {
    for (auto &n1 : nodes()) {
        for (auto &n2 : nodes()) {
            if (n1 != n2)
                if (BFS(n1, n2).empty())
                    return false;
        }
    }
    return true;
}
