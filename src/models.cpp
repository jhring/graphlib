#include <random>
#include <iostream>
#include <numeric>
#include <algorithm>

#include "../include/models.h"

Graph complete_graph(unsigned int n) {
    std::map<int, std::set<int>> adjList;

    std::set<int> nodes;
    for (int i = 0; i < n; ++i) {
        nodes.insert(i);
    }

    for (int i = 0; i < n; ++i) {
        adjList[i] = nodes;
        adjList[i].erase(i);
    }
    return Graph(adjList);
}

Graph er_model(unsigned int n, unsigned int M) {
    if (M > (n * (n-1) / 2)) {
        std::cerr << "Error: er_model, M = " << M << " is too large for " << n << "\n";
        std::_Exit(1);
    }
    Graph g;
    std::mt19937 gen;

    for (int i = 0; i < n; ++i) {
        g.add_node(i);
    }
    if (M < (n * (n-1) / 4)) {
        for (int i = 0; i < M; ++i) {
            edge e(static_cast<int>(gen() % n), static_cast<int>(gen() % n));
            if ( (e.first == e.second) || g.has_edge(e))
                --i;
            else
                g.add_edge(e);
        }
    }
    else {
        g = complete_graph(n);
        auto edges = g.edges();

        while (edges.size() != M) {
            auto idx = (int) (gen() % edges.size());
            g.remove_edge(edges[idx]);
            edges.erase(edges.begin() + idx);
        }
    }
    return g;
}

Graph configuration_model(std::vector<unsigned long> deg_sequence) {
    unsigned int M =  std::accumulate(deg_sequence.begin(), deg_sequence.end(), 0);
    unsigned long N = deg_sequence.size();

    if (M % 2 != 0 || M > (N * (N-1) / 2)) {
        std::cerr << "Error: configuration_model, Invalid degree sequence.\n";
        std::_Exit(1);
    }
    std::mt19937 gen;
    Graph g;

    for (int i = 0; i < N; ++i)
        g.add_node(i);

    if (N == 0) {
        return g;
    }

    std::vector<unsigned int> stublist;
    stublist.reserve(M);
    for (int i = 0; i < N; ++i) {
        for (int j = 0; j < deg_sequence[i]; ++j) {
            stublist.push_back(i);
        }
    }

    std::shuffle(stublist.begin(), stublist.end(), gen);
    while (!stublist.empty()) {
        int e1 = stublist.back();
        stublist.pop_back();
        int e2 = stublist.back();
        stublist.pop_back();
        g.add_edge({e1, e2});
    }
    return g;
}